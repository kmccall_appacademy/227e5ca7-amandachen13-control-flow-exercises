# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercase = ""
  str.each_char do |char|
    if char == char.downcase
      lowercase += char
    end
  end
  str.delete!(lowercase)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str
  while middle.length > 2
    middle = middle[1..-2]
  end
  middle
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count(VOWELS.join)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  fact = 1
  (1..num).each do |num|
    fact *= num
  end
  fact
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_str = ""
  arr.each_with_index do |el, idx|
    joined_str += el
    joined_str += separator if idx < (arr.length - 1)
  end
  joined_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""
  str.each_char.with_index do |char, idx|
    if idx.even?
      weird_str += char.downcase
    else
      weird_str += char.upcase
    end
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  str.split.each do |word|
    if word.length >= 5
      result << word.reverse
    else
      result << word
    end
  end
  result.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  arr.each_with_index do |num, idx|
    if num % 15 == 0
      arr[idx] = "fizzbuzz"
    elsif num % 3 == 0
      arr[idx] = "fizz"
    elsif num % 5 == 0
      arr[idx] = "buzz"
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  arr.each {|el| reverse_arr.unshift(el)}
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  num > 1 && (2..num).count {|n| num % n == 0} == 1
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  sorted_factors = []
  (1..num).each do |n|
    sorted_factors << n if num % n == 0
  end
  sorted_factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|n| prime?(n)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = arr.select {|num| num.even?}
  odd = arr.select {|num| num.odd?}

  return odd[0] if odd.count == 1
  return even[0] if even.count == 1
end
